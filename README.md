# FileManager

# Deployment
<p>Extract the archive and put it in the folder you want</p>
<p>Run <b>cp .env.example .env</b> file to copy example file to .env</p> 
<p>Then edit your .env file with DB credentials and other settings.</p>
<p>Run <b>composer install</b> command</p>
<p><b>composer upgrade intervention/image</b></p>
<p><b>composer require "spatie/laravel-medialibrary:^9.4.3"</b></p>
<p>Run <b>php artisan migrate --seed</b> command.</p> 
<p>Notice: seed is important, because it will create the first admin user for you.</p>
<p>Run <b>php artisan key:generate</b> command.</p>
<p>If you have file/photo fields, run <b>php artisan storage:link</b> command.</p>
<p>And that's it, go to your domain and login:</p>

# Default credentials
<p>Username: admin@admin.com
<p>Password: password
