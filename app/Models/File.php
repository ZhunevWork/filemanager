<?php

namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use kornrunner\Blurhash\Blurhash;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use \DateTimeInterface;

class File extends Model implements HasMedia
{
    use InteractsWithMedia, HasFactory;

    public $table = 'files';

    protected $appends = [
        'data',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'folder_id',
        'blure_hash',
        'file_hash',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('origin')->format('webp');
        $this->addMediaConversion('origin')->format('jpg');
        $consersionArray = Config::get('media');
        foreach ($consersionArray as $convert){
            $this->addMediaConversion($convert['name'])->fit($convert['method'], $convert['width'], $convert['height'])->format($convert['format']);
        }
    }

    public function getBlureHash($file) : String {
        $fileForBlur  = $file;
        $image = imagecreatefromstring(file_get_contents($fileForBlur));
        $width = imagesx($image);
        $height = imagesy($image);

        $pixels = [];
        for ($y = 0; $y < $height; ++$y) {
            $row = [];
            for ($x = 0; $x < $width; ++$x) {
                $index = imagecolorat($image, $x, $y);
                $colors = imagecolorsforindex($image, $index);

                $row[] = [$colors['red'], $colors['green'], $colors['blue']];
            }
            $pixels[] = $row;
        }

        $components_x = 4;
        $components_y = 3;

        $blurhash = Blurhash::encode($pixels, $components_x, $components_y);

        return $blurhash;
    }

    public function getDataAttribute()
    {
        $file = $this->getMedia('data')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->preview   = $file->getUrl('preview');
        }

        return $file;
    }

    public function folder()
    {
        return $this->belongsTo(Folder::class, 'folder_id');
    }
}
