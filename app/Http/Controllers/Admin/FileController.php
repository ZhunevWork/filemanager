<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyFileRequest;
use App\Http\Requests\StoreFileRequest;
use App\Http\Requests\UpdateFileRequest;
use App\Models\File;
use App\Models\Folder;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class FileController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('file_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $files = File::with(['folder', 'media'])->get();

        $folders = Folder::get();

        return view('admin.files.index', compact('files', 'folders'));
    }

    public function create()
    {
        abort_if(Gate::denies('file_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $folders = Folder::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.files.create', compact('folders'));
    }

    public function store(StoreFileRequest $request)
    {
        $file = File::create($request->all());

        if ($request->input('data', false)) {
            $file->blure_hash = $file->getBlureHash(storage_path('tmp/uploads/' . basename($request->input('data'))));
            $file->addMedia(storage_path('tmp/uploads/' . basename($request->input('data'))))->toMediaCollection('data');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $file->id]);
        }
        $file->save();
        return redirect()->route('admin.files.index');
    }

    public function edit(File $file)
    {
        abort_if(Gate::denies('file_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $folders = Folder::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $file->load('folder');

        return view('admin.files.edit', compact('folders', 'file'));
    }

    public function update(UpdateFileRequest $request, File $file)
    {
        $file->update($request->all());

        if ($request->input('data', false)) {
            if (!$file->data || $request->input('data') !== $file->data->file_name) {
                if ($file->data) {
                    $file->data->delete();
                }
                $file->blure_hash = $file->getBlureHash(storage_path('tmp/uploads/' . basename($request->input('data'))));
                $file->addMedia(storage_path('tmp/uploads/' . basename($request->input('data'))))->toMediaCollection('data');
            }
        } elseif ($file->data) {
            $file->data->delete();
        }

        $file->save();
        return redirect()->route('admin.files.index');
    }

    public function show(File $file)
    {
        abort_if(Gate::denies('file_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $file->load('folder');

        return view('admin.files.show', compact('file'));
    }

    public function destroy(File $file)
    {
        abort_if(Gate::denies('file_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $file->delete();

        return back();
    }

    public function massDestroy(MassDestroyFileRequest $request)
    {
        File::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('file_create') && Gate::denies('file_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new File();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
