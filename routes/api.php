<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:sanctum']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Folders
    Route::apiResource('folders', 'FolderApiController');

    // Files
    Route::post('files/media', 'FileApiController@storeMedia')->name('files.storeMedia');
    Route::apiResource('files', 'FileApiController');
});
