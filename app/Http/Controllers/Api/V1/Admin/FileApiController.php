<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreFileRequest;
use App\Http\Requests\UpdateFileRequest;
use App\Http\Resources\Admin\FileResource;
use App\Models\File;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FileApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('file_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FileResource(File::with(['folder'])->get());
    }

    public function store(StoreFileRequest $request)
    {
        $file = File::create($request->all());

        if ($request->input('data', false)) {
            $file->blure_hash = $file->getBlureHash(storage_path('tmp/uploads/' . basename($request->input('data'))));
            $file->addMedia(storage_path('tmp/uploads/' . basename($request->input('data'))))->toMediaCollection('data');
        }

        $file->save();
        return (new FileResource($file))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(File $file)
    {
        abort_if(Gate::denies('file_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FileResource($file->load(['folder']));
    }

    public function update(UpdateFileRequest $request, File $file)
    {
        $file->update($request->all());

        if ($request->input('data', false)) {
            if (!$file->data || $request->input('data') !== $file->data->file_name) {
                if ($file->data) {
                    $file->data->delete();
                }
                $file->blure_hash = $file->getBlureHash(storage_path('tmp/uploads/' . basename($request->input('data'))));
                $file->addMedia(storage_path('tmp/uploads/' . basename($request->input('data'))))->toMediaCollection('data');
            }
        } elseif ($file->data) {
            $file->data->delete();
        }

        $file->save();
        return (new FileResource($file))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(File $file)
    {
        abort_if(Gate::denies('file_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $file->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
