<?php
return [
    ['name' => "thumb", 'method' => 'crop', 'width' => '50', 'height' => '50', 'format' => 'jpg'],
    ['name' => "preview", 'method' => 'crop', 'width' => '120', 'height' => '120', 'format' => 'jpg'],
    ['name' => "200x300", 'method' => 'crop', 'width' => '200', 'height' => '300', 'format' => 'jpg'],
    ['name' => "200x300", 'method' => 'crop', 'width' => '200', 'height' => '300', 'format' => 'png'],
    ['name' => "200x300", 'method' => 'crop', 'width' => '200', 'height' => '300', 'format' => 'webp'],
    ['name' => "600x800", 'method' => 'crop', 'width' => '600', 'height' => '800', 'format' => 'jpg'],
    ['name' => "600x800", 'method' => 'crop', 'width' => '600', 'height' => '800', 'format' => 'png'],
    ['name' => "600x800", 'method' => 'crop', 'width' => '600', 'height' => '800', 'format' => 'webp'],
];
