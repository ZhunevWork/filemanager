@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.file.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.files.update", [$file->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.file.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $file->name) }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.file.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="data">{{ trans('cruds.file.fields.data') }}</label>
                <div class="needsclick dropzone {{ $errors->has('data') ? 'is-invalid' : '' }}" id="data-dropzone">
                </div>
                @if($errors->has('data'))
                    <span class="text-danger">{{ $errors->first('data') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.file.fields.data_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="folder_id">{{ trans('cruds.file.fields.folder') }}</label>
                <select class="form-control select2 {{ $errors->has('folder') ? 'is-invalid' : '' }}" name="folder_id" id="folder_id" required>
                    @foreach($folders as $id => $folder)
                        <option value="{{ $id }}" {{ (old('folder_id') ? old('folder_id') : $file->folder->id ?? '') == $id ? 'selected' : '' }}>{{ $folder }}</option>
                    @endforeach
                </select>
                @if($errors->has('folder'))
                    <span class="text-danger">{{ $errors->first('folder') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.file.fields.folder_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="blure_hash">{{ trans('cruds.file.fields.blure_hash') }}</label>
                <input class="form-control {{ $errors->has('blure_hash') ? 'is-invalid' : '' }}" type="text" name="blure_hash" id="blure_hash" value="{{ old('blure_hash', $file->blure_hash) }}">
                @if($errors->has('blure_hash'))
                    <span class="text-danger">{{ $errors->first('blure_hash') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.file.fields.blure_hash_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="file_hash">{{ trans('cruds.file.fields.file_hash') }}</label>
                <input class="form-control {{ $errors->has('file_hash') ? 'is-invalid' : '' }}" type="text" name="file_hash" id="file_hash" value="{{ old('file_hash', $file->file_hash) }}">
                @if($errors->has('file_hash'))
                    <span class="text-danger">{{ $errors->first('file_hash') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.file.fields.file_hash_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.dataDropzone = {
    url: '{{ route('admin.files.storeMedia') }}',
    maxFilesize: 512, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif,.webp',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 512,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="data"]').remove()
      $('form').append('<input type="hidden" name="data" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="data"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($file) && $file->data)
      var file = {!! json_encode($file->data) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, file.preview)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="data" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection
